package PageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBys;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import static java.time.temporal.ChronoUnit.SECONDS;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

@DefaultUrl("http://www.renfe.com/")

public class RenfeHome extends PageObject {
    @FindBy(xpath = "//*[@id=\"IdOrigen\"]")
    private WebElementFacade Origin;

    @FindBys({
            @FindBy(xpath = "//*[@id=\"ui-id-1\"]"),
            @FindBy(tagName = "li")
    })
    private WebElementFacade Selection;

    @FindBy(xpath = "//*[@id=\"IdDestino\"]")
    private WebElementFacade Destination;

    @FindBys({
            @FindBy(xpath = "//*[@id=\"ui-id-2\"]"),
            @FindBy(tagName = "li")
    })
    private WebElementFacade Selection2;

    @FindBy(xpath = "//*[@id=\"__fechaIdaVisual\"]")
    private WebElementFacade DateOut;

    @FindBy(xpath = "//*[@id=\"__fechaVueltaVisual\"]")
    private WebElementFacade DateBack;

    @FindBy(xpath = "//*[@id=\"__numAdultos\"]")
    private WebElementFacade AdultsNumber;

    @FindBy(xpath = "//*[@id=\"__numNinos\"]")
    private WebElementFacade ChildrenNumber;

    @FindBy(xpath = "//*[@id=\"datosBusqueda\"]/div[4]/fieldset[2]/div/label")
    private WebElementFacade Pets;

    @FindBy(xpath = "//*[@id=\"datosBusqueda\"]/button")
    private WebElementFacade submit;


    SimpleDateFormat europeanDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Calendar currentDate = Calendar.getInstance();

    public RenfeHome(WebDriver driver) {
        super(driver);
        maximize(driver);
    }

    private void maximize(WebDriver driver) {
        driver.manage().window().maximize();
    }

    public void writeOriginAndDestination(String Origin, String Destination) {
        write(this.Origin, Origin);
        select(this.Selection);
        write(this.Destination, Destination);
        select(this.Selection2);
    }

    public void openPage() {
        this.setImplicitTimeout(15, SECONDS);
        this.open();
        waitLoad();
    }

    public void waitLoad() {
        this.getDriver().manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
    }

    /**
     * Add <code>daysAfterCurrentDate</code> to current date and type the result.
     *
     * @param element              webelement
     * @param daysAfterCurrentDate Number of days we want to add.
     */
    private void writeDaysAfterCurrentDate(WebElementFacade element, int daysAfterCurrentDate) {
        element.waitUntilClickable();
        element.clear();
        currentDate.add(Calendar.DATE, daysAfterCurrentDate);
        String Data = europeanDateFormat.format(currentDate.getTime());
        element.type(Data);
    }

    /**
     * Add <code>daysAfterDeparture</code> to departure date and type the result.
     *
     * @param element            Locator
     * @param daysAfterDeparture Number of days we want to add.
     */
    private void writeDaysAfterDeparture(WebElementFacade element, int daysAfterDeparture) {
        element.waitUntilClickable();
        element.clear();
        currentDate.add(Calendar.DATE, daysAfterDeparture);
        String Data = europeanDateFormat.format(currentDate.getTime());
        element.type(Data);
    }

    private void write(WebElementFacade element, String Data) {
        element.waitUntilClickable();
        element.clear();
        element.type(Data);
    }

    private void select(WebElementFacade element) {
        element.waitUntilClickable();
        element.click();
    }

    public void writeDateOut(int DateOut) {
        writeDaysAfterCurrentDate(this.DateOut, DateOut);
    }

    public void writeDateBack(int DateBack) {
        writeDaysAfterDeparture(this.DateBack, DateBack);
    }

    public void writeNumberOfPassengers(String AdultsNumber, String ChildrenNumber) {
        write(this.AdultsNumber, AdultsNumber);
        Assert.assertEquals(AdultsNumber, this.AdultsNumber.getValue());
        write(this.ChildrenNumber, ChildrenNumber);
        Assert.assertEquals(ChildrenNumber, this.ChildrenNumber.getValue());

    }
        public void SelectCheckBox (String mascota){
            if (mascota.equals("Checked")) {
                Pets.waitUntilClickable();
                Pets.click();
                Assert.assertEquals(mascota, "Checked");
            } else {
                Assert.assertNotEquals(mascota, "Checked");
            }
        }

        public RenfeResults submitSearch (String mascota){
            submit.click();
            if (mascota.equals("Checked")) {
                this.waitFor(presenceOfElementLocated(By.id("HorariosIda")));
                this.waitFor(presenceOfElementLocated(By.id("Horarios Vuelta")));
            } else {

                this.waitFor(visibilityOfElementLocated(By.id("lab-trayecto0")));
                this.waitFor(visibilityOfElementLocated(By.id("lab-trayecto1")));
            }
            return new RenfeResults(this.getDriver());
        }

}
