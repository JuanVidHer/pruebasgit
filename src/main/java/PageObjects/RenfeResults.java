package PageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class RenfeResults extends PageObject {
    @FindAll({@FindBy(xpath= "//*[@id=\"tablaTrenesIda\"]/tbody")})
    private List<WebElementFacade> avalaibleRides;

    public RenfeResults( WebDriver driver) {
        super (driver);
    }
    public int resultsNum(){
        return avalaibleRides.size();
    }

}
