package Steps.Serenity;
import PageObjects.RenfeHome;
import PageObjects.RenfeResults;
import net.thucydides.core.annotations.Step;

public class RenfeStepsDefinitions {

        private RenfeHome renfeHome;
        private RenfeResults renfeResults;
        private String petValue;
        private String controlPet;


        @Step("Given I have to enter Renfe page")
        public void RenfeMainPageLoaded() {
            renfeHome.openPage();
            renfeHome.waitLoad();
        }

        @Step("I search a ride from \"<Origin>\" to \"<Destination>\"")
        public void ISelectOriginAndDestination (String Origin, String Destination){
               renfeHome.writeOriginAndDestination(Origin,Destination);
        }

        @Step("And with dates: \"<DateOut>\" and \"<DateBack>\"")
        public void withDates(int DateOut, int DateBack){
                renfeHome.writeDateOut(DateOut);
                renfeHome.writeDateBack(DateBack);
        }

        @Step ("And \"<AdultsNumber>\" adults and \"<ChildrenNumber>\" children")
        public void PassengerNumber(String AdultsNumber, String ChildrenNumber){
                renfeHome.writeNumberOfPassengers (AdultsNumber,ChildrenNumber);
        }

        @Step("And with or without \"<Pet>\" and I submit")
        public void With_or_without_pet(String petValue){
                this.petValue = petValue;
                renfeHome.SelectCheckBox(petValue);
                renfeHome.submitSearch(petValue);
        }

        @Step("Then I should get \"<Rows>\" rows")
        public void I_Should_get_rows (int rows){}
    }

