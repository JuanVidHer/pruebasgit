package Steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import Steps.Serenity.RenfeStepsDefinitions;

public class StepsDefinitions {

    @Steps
    RenfeStepsDefinitions client;

    @Given ("^Renfe main page is loaded$")
    public void RenfeMainPageLoaded() {
        client.RenfeMainPageLoaded();
    }

    @When ("^I select Origin: '(.*)' station and Destination: '(.*)' station$")
    public void ISelectOriginAndDestination (String Origin, String Destination){
        client.ISelectOriginAndDestination (Origin, Destination);
    }


    @And("^with dates: '(.*)' days after actual date and '(.*)' days after date out$")
    public void WithDates(int DateOut, int DateBack){
        client.withDates(DateOut, DateBack);
    }

    @And ("^I select '(.*)' adults and '(.*)' children$")
    public void PassengerNumber(String AdultsNumber, String ChildrenNumber){
        client.PassengerNumber(AdultsNumber, ChildrenNumber);
    }

    @And("^with or without '(.*)' and I submit$")
    public void WithOrWithoutPet(String Pet){
        client.With_or_without_pet(Pet);
    }

    @Then("^I should get '(.*) rows")
    public void IShouldGetRows (int Rows){
        client.I_Should_get_rows (Rows);
    }
}




