package StepsPets;

import StepsPets.Serenity.PetStepsDefinitions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class StepsDefinitions {

    @Steps
    private PetStepsDefinitions petStepsDefinitions;

    /*
    GET
     */

    @When("^I request to get a user by id '(.*)'$")
    public void IRequestToGetAUserID (int id){
        petStepsDefinitions.getUserByID(id);
    }

    @Then("^I should get expected code '(.*)'$")
    public void IShouldGet(int expectedCode){
         petStepsDefinitions.verifyStatusCode(expectedCode);
    }

    @And("^The value for '(.*)' after the get operation should be '(.*)'$")
    public void TheValueForAfterTheGetOperationShouldBe (String parameter, String expectedValue){
        petStepsDefinitions.verifyParameterValue(parameter, expectedValue);

    }

    /*
    POST
     */

    @When("^I request to do a (POST|GET|PUT|DELETE) operation with '(.*)' and '(.*)'$")
    public void IrequestToPost(String method,String headerField, String headerValue){
        petStepsDefinitions.setHeader(method,headerField, headerValue);
    }
    @And ("^with URI '(.*)'$")
    public void WithURI(String uri) {
        petStepsDefinitions.setPath(uri);
    }

    @And ("^with body '(.*)'$")
    public void WithBody(String jsonBodyFile) {
        petStepsDefinitions.withBody(jsonBodyFile);
    }

    @Then ("^I should get post expected code '(.*)'$")
    public void IShouldGetPost(int StatusCode){
        petStepsDefinitions.verifyStatusCode(StatusCode);
    }

    @And("^response body equals to '(.*)'$")
    public void ResponseBodyEquals (String expectedJsonBody){
        petStepsDefinitions.verifyBody(expectedJsonBody);
    }
    /*
    PUT
     */

    /*
    DELETE
     */
    @And("^delete pet with id equals to '(.*)'$")
    public void deletePetWithIDEqualsTo(String id){
        petStepsDefinitions.deleteByID(id);
    }
}
