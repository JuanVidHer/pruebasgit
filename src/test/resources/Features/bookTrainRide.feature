#Posible values for variable Pet: Checked/Unchecked
  @renfe
Feature: Search in Renfe for available rides from Origin to Destination

  Scenario Outline: Search for ride in Renfe
    Given Renfe main page is loaded
    When I select Origin: '<Origin>' station and Destination: '<Destination>' station
    And  with dates: '<DateOut>' days after actual date and '<DateBack>' days after date out
    And  I select '<AdultsNumber>' adults and '<ChildrenNumber>' children
    And  with or without '<Pet>' and I submit
    Then I should get '<Num>' rows

    Examples:
      | Origin     | Destination | DateOut | DateBack | AdultsNumber | ChildrenNumber | Pet       | Num |
      | Valladolid | Barcelona   | 3       | 5        | 1            | 2              | Unchecked | 1   |
      | Salamanca  | Palencia    | 2       | 4        | 2            | 3              | Checked   | 2   |