Feature: Check details about a pet in the store

  @GET

  Scenario Outline: Get an existing pet in the store

    When I request to get a user by id '<userID>'
    Then I should get expected code '<expectedCode>'
    And  The value for '<parameter>' after the get operation should be '<parameterValue>'

    Examples:
      | userID | expectedCode | parameter | parameterValue |
      | 4      | 200          | name      | perro          |

  @JSON @POST @2

  Scenario Outline: Post a new pet in the store

    When I request to do a POST operation with '<field>' and '<fieldValue>'
    And  with URI '/pet'
    And  with body '/Requests/post_pet_<expectedStatusCode>.json'
    Then I should get post expected code '<expectedStatusCode>'
    And response body equals to '/Requests/expected_post_pet_<expectedStatusCode>.json'

    Examples:
      | field        | fieldValue       | expectedStatusCode |
      | Content-Type | application/json | 200                |

  @XML @POST

  Scenario Outline: Post a new pet in the store with xml

    When I request to do a POST operation with '<field>' and '<fieldValue>'
    And  with URI '/pet'
    And  with body '/Requests/post_pet_<expectedStatusCode>.xml'
    Then I should get post expected code '<expectedStatusCode>'
    And response body equals to '/Requests/expected_post_pet_<expectedStatusCode>.xml'

    Examples:
      | field        | fieldValue      | expectedStatusCode |
      | Content-Type | application/xml | 200                |

  @JSON @PUT @2
  Scenario Outline: PUT a new pet for the store

    When I request to do a PUT operation with '<field>' and '<fieldValue>'
    And with URI '/pet'
    And with body '/Requests/put_pet_<expectedStatusCode>.json'
    Then I should get post expected code '<expectedStatusCode>'
    And response body equals to '/Requests/expected_put_pet_<expectedStatusCode>.json'
    Examples:
      | field        | fieldValue            | expectedStatusCode |
      | Content-Type | application/json | 200                |

  @JSON @DELETE @1
  Scenario Outline: Delete a pet in the store with json

    When I request to do a DELETE operation with '<field>' and '<fieldValue>'
    And with URI '/pet'
    And delete pet with id equals to '<deleteId>'
    Then I should get expected code '<expectedStatusCode>'

    Examples:
      | field        | fieldValue       | deleteId | expectedStatusCode |
      | Content-Type | application/json | 888      | 200                |
